package org.elu.learn.fullstack.spring.boot3.cardatabasenew

import org.assertj.core.api.Assertions.assertThat
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.web.CarController
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class CarDatabaseNewApplicationTests {
    @Autowired
    lateinit var carController: CarController

    @Test
    fun contextLoads() {
        assertThat(carController).isNotNull
    }
}

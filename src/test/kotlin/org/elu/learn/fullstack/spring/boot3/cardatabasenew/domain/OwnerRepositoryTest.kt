package org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain

import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.longs.shouldBeExactly
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class OwnerRepositoryTest {
    @Autowired
    lateinit var ownerRepository: OwnerRepository

    @Test
    fun `should successfully save owner`() {
        ownerRepository.save(Owner(firstName = "Matti", lastName =  "Meikläinen"))

        val result = ownerRepository.findByFirstName("Matti")
        assertThat(result).isNotNull
    }

    @Test
    fun `should successfully delete all owners`() {
        ownerRepository.save(Owner(firstName =  "Antti", lastName =  "Testaaja"))

        val owners = ownerRepository.findAll()
        owners shouldHaveSize 1

        ownerRepository.deleteAll()
        ownerRepository.count() shouldBeExactly 0
    }
}

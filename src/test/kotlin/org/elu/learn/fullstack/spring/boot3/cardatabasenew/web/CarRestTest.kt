package org.elu.learn.fullstack.spring.boot3.cardatabasenew.web

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class CarRestTest(@Autowired val mockMvc: MockMvc) {
    @Test
    fun `should successfully authenticate`() {
        this.mockMvc
            .perform(post("/login")
                .content("""{"username":"admin","password":"admin"}""")
                .header(HttpHeaders.CONTENT_TYPE, "application/json"))
            .andDo(print())
            .andExpect(status().isOk())
    }
}

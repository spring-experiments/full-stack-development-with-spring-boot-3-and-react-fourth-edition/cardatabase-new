package org.elu.learn.fullstack.spring.boot3.cardatabasenew.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.CarDatabaseNewApplication
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.AppUser
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.AppUserRepository
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.Car
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.CarRepository
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.Owner
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.OwnerRepository
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class ApplicationConfiguration {
    @Bean
    fun carDatabaseOpenAPI(): OpenAPI =
        OpenAPI()
            .info(Info()
                .title("Car REST API")
                .description("My car stock")
                .version("1.0")
            )

    @Bean
    fun databaseInitializer(
        repository: CarRepository,
        ownerRepository: OwnerRepository,
        appUserRepository: AppUserRepository,
        encoder: PasswordEncoder
    ) = ApplicationRunner {
        val owner1 = Owner(firstName = "Ed", lastName = "Suisse", cars = listOf())
        val owner2 = Owner(firstName = "Janna", lastName = "Suisse", cars = listOf())
        ownerRepository.saveAll(listOf(owner1, owner2))

        repository.save(
            Car(
                brand = "Mercedes",
                model = "GLA 220",
                color = "Black",
                registrationNumber = "ABC-1234",
                modelYear = 2019,
                price = 32000,
                owner = owner1,
            )
        )
        repository.save(
            Car(
                brand = "Mercedes",
                model = "GLC 320",
                color = "Silver",
                registrationNumber = "ABC-2345",
                modelYear = 2020,
                price = 42000,
                owner = owner1,
            )
        )
        repository.save(
            Car(
                brand = "Volvo",
                model = "XC-90",
                color = "White",
                registrationNumber = "ABC-3456",
                modelYear = 2020,
                price = 52000,
                owner = owner2,
            )
        )

        repository.findAll().forEach { CarDatabaseNewApplication.logger.info("${it.brand} ${it.model}") }

        // Username: user, password: user
        appUserRepository.save(AppUser(
            username = "user",
            password = encoder.encode("user"),
            role = "USER"))
        // Username: admin, password: admin
        appUserRepository.save(AppUser(
            username = "admin",
            password = encoder.encode("admin"),
            role = "ADMIN"))
    }
}

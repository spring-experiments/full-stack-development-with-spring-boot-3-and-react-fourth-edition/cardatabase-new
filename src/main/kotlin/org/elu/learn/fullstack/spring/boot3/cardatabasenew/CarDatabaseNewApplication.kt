package org.elu.learn.fullstack.spring.boot3.cardatabasenew

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity

@SpringBootApplication
@EnableMethodSecurity
class CarDatabaseNewApplication {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(CarDatabaseNewApplication::class.java)
    }
}

fun main(args: Array<String>) {
    runApplication<CarDatabaseNewApplication>(*args)
    CarDatabaseNewApplication.logger.info("Application started")
}

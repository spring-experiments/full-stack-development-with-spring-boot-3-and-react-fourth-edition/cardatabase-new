package org.elu.learn.fullstack.spring.boot3.cardatabasenew.web

import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.AccountCredentials
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.service.JwtService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class LoginController(
    private val jwtService: JwtService,
    private val authenticationManager: AuthenticationManager
) {
    @PostMapping("/login")
    fun login(@RequestBody credentials: AccountCredentials): ResponseEntity<Void> {
        val auth = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(credentials.username, credentials.password)
        )
        val token: String = jwtService.getToken(auth.name)
        return ResponseEntity.ok()
            .header(HttpHeaders.AUTHORIZATION, "Bearer $token")
            .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "Authorization")
            .build()
    }
}

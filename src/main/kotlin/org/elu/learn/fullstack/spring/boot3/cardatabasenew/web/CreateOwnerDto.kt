package org.elu.learn.fullstack.spring.boot3.cardatabasenew.web

data class CreateOwnerDto(
    val firstName: String,
    val lastName: String
)

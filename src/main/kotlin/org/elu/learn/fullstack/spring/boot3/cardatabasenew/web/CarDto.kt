package org.elu.learn.fullstack.spring.boot3.cardatabasenew.web

data class CarDto(
    val id: Long? = null,
    val brand: String,
    val model: String,
    val color: String,
    val registrationNumber: String,
    val modelYear: Int,
    val price: Int,
    val owner: String? = null,
)

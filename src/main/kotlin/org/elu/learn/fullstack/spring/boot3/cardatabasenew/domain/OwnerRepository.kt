package org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain

import org.springframework.data.repository.CrudRepository

interface OwnerRepository : CrudRepository<Owner, Long> {
    fun findByFirstName(firstName: String): Owner?
}

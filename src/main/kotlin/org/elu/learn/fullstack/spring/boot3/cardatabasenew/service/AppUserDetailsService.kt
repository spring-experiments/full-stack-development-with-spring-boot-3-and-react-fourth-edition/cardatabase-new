package org.elu.learn.fullstack.spring.boot3.cardatabasenew.service

import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.AppUser
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.AppUserRepository
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class AppUserDetailsService(
    private val appUserRepository: AppUserRepository
) : UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails {
        val user: AppUser? = appUserRepository.findByUsername(username)
        return user?.let { currentUser ->
            User
                .withUsername(currentUser.username)
                .password(currentUser.password)
                .roles(currentUser.role)
                .build()
        } ?: throw UsernameNotFoundException("User not found.")
    }
}

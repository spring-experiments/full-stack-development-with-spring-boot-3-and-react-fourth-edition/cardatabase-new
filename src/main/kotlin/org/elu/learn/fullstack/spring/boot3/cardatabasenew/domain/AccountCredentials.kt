package org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain

data class AccountCredentials(
    val username: String,
    val password: String,
)

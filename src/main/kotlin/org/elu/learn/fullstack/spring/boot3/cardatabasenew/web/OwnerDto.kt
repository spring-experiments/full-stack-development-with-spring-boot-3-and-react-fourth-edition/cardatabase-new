package org.elu.learn.fullstack.spring.boot3.cardatabasenew.web

data class OwnerDto(
    val id: Long? = null,
    val firstName: String,
    val lastName: String,
    val cars: List<CarDto>,
)

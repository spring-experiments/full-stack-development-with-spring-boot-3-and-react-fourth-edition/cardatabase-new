package org.elu.learn.fullstack.spring.boot3.cardatabasenew.web

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/owners")
class OwnerController(private val ownerService: OwnerService) {
    @GetMapping
    fun getOwners() = ownerService.getAll()

    @GetMapping("{id}")
    fun getOwner(@PathVariable id: Long): OwnerDto = ownerService.getById(id)

    @PostMapping
    fun createOwner(@RequestBody owner: CreateOwnerDto) = ownerService.create(owner)

    @PutMapping("{id}")
    fun updateOwner(@PathVariable id: Long, @RequestBody owner: CreateOwnerDto) = ownerService.update(id, owner)

    @DeleteMapping("{id}")
    fun delete(@PathVariable id: Long) = ownerService.delete(id)
}

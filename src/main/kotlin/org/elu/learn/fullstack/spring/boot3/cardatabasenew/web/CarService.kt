package org.elu.learn.fullstack.spring.boot3.cardatabasenew.web

import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.CarRepository
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.Owner
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.OwnerRepository
import org.springframework.stereotype.Service

@Service
class CarService(private val carRepository: CarRepository,
                 private val ownerRepository: OwnerRepository,
                 private val mapper: AppMapper) {
    fun getAll() = carRepository.findAll().map { mapper.map(it) }

    fun getById(id: Long): CarDto = carRepository.findById(id)
        .map { mapper.map(it) }
        .orElseThrow { throw IllegalArgumentException("not found") }

    fun create(car: CarDto): CarDto {
        val owner = car.owner?.let { getOwner(it) }
        val created = carRepository.save(mapper.map(car, owner))
        return mapper.map(created)
    }

    fun update(id: Long, car: CarDto): CarDto {
        val owner = car.owner?.let { getOwner(it) }
        val found = carRepository.findById(id).orElseThrow { throw IllegalArgumentException("not found") }
        found.color = car.color
        found.registrationNumber = car.registrationNumber
        found.price = car.price
        found.owner = owner
        val saved = carRepository.save(found)
        return mapper.map(saved)
    }

    fun delete(id: Long) {
        carRepository.findById(id).orElseThrow { throw IllegalArgumentException("not found") }
        carRepository.deleteById(id)
    }

    private fun getOwner(id: String): Owner {
        val ownerId = id.toLong()
        return ownerRepository.findById(ownerId).orElseThrow { throw IllegalArgumentException("no owner found") }
    }
}

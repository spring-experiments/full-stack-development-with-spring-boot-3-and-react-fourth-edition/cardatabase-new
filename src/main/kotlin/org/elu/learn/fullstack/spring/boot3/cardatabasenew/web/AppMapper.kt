package org.elu.learn.fullstack.spring.boot3.cardatabasenew.web

import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.Car
import org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain.Owner
import org.springframework.stereotype.Component

@Component
class AppMapper {
    fun map(car: Car) =
        CarDto(
            id = car.id,
            brand = car.brand,
            model = car.model,
            color = car.color,
            registrationNumber = car.registrationNumber,
            modelYear = car.modelYear,
            price = car.price,
            owner = car.owner?.let { "${it.firstName} ${it.lastName}" }
        )

    fun map(car: CarDto, owner: Owner?) =
        Car(
            id = car.id,
            brand = car.brand,
            model = car.model,
            color = car.color,
            registrationNumber = car.registrationNumber,
            modelYear = car.modelYear,
            price = car.price,
            owner = owner
        )

    fun map(owner: Owner) =
        OwnerDto(
            id = owner.id,
            firstName = owner.firstName,
            lastName = owner.lastName,
            cars = owner.cars.map { map(it) }
        )

    fun map(owner: CreateOwnerDto) =
        Owner(
            firstName = owner.firstName,
            lastName = owner.lastName,
            cars = listOf(),
        )
}

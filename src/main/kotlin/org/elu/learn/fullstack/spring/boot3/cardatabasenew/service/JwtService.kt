package org.elu.learn.fullstack.spring.boot3.cardatabasenew.service

import io.jsonwebtoken.Jwts
import org.springframework.stereotype.Service
import java.util.*

@Service
class JwtService {
    fun getToken(username: String,
                 additionalClaims: Map<String, Any> = emptyMap()): String =
        Jwts.builder()
            .claims()
            .subject(username)
            .expiration(Date(System.currentTimeMillis() + EXPIRATION_TIME))
            .add(additionalClaims)
            .and()
            .signWith(KEY)
            .compact()

    fun getAuthUser(token: String): String? =
        Jwts.parser()
            .verifyWith(KEY)
            .build()
            .parseSignedClaims(token)
            .payload
            .subject

    companion object {
        const val EXPIRATION_TIME: Long = 86400000   // 1 day in ms. Should be shorter in production.

        // Generate secret key. Only for demonstration purposes.
        // In production, you should read it from the application configuration.
        private val KEY = Jwts.SIG.HS256.key().build()
    }
}

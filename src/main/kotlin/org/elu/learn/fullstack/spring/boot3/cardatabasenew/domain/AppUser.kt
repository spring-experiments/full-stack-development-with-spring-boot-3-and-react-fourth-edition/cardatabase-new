package org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id

@Entity
class AppUser(
    @Id
    @GeneratedValue
    @Column(nullable = false, updatable = false)
    val id: Long? = null,
    @Column(nullable = false, unique = true)
    val username: String,
    @Column(nullable = false)
    val password: String,
    @Column(nullable = false)
    val role: String
)

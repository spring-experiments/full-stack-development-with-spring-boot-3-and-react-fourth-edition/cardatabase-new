package org.elu.learn.fullstack.spring.boot3.cardatabasenew.domain

import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne

@Entity
class Car(
    @Id
    @GeneratedValue
    val id: Long? = null,
    val brand: String,
    val model: String,
    var color: String,
    var registrationNumber: String,
    val modelYear: Int,
    var price: Int,
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner")
    var owner: Owner?,
)
